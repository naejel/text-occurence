# Text Occurence

Count occurrences in UTF-8 text files (not work with PDF,Binary, etc.)

## What it does

Can parse one file or all files in a directory

Count occurence of a given letter

Count occurence of all letters

Count occurence of a given word

Count occcurence of all words

Count occurence of size given polygram

Save result as CSV file

## List of args

## Example

Feel free to use the --help options

## TODO

### More Atomic Class

- [x] No need of parse list of files in class?
- [x] Remove sortedDict of class
- [x] remove countSequence of class
- [x] Better name for keepNonAlphaNum -> nonAlNum

### Improve Class

- [ ] Add raise to methods
- [x] countPolygrams params
- [x] Refactor argument parsing
- [x] Improve dicts
- [ ] Letters are polygrams of size 1

New dict store result looks like:

```json
{
 "file1": {
  "letters": {
   "X": {
     "number": 3,
     "position": [1, 2, 3]
   },
   "Y": {
     "number": 3,
     "position": [1, 2, 3]
   }
  },
  "Words": {
    "hello": {
      "number": 1,
      "position": [0]
    },
    "world": {
      "number": 1,
      "position": [7]
    }
  }
 },
 "file2": {
  "letters": {
   "X": {
     "number": 3,
     "position": [1, 2, 3]
   },
   "Y": {
     "number": 3,
     "position": [1, 2, 3]
   }
  },
  "Words": {
    "hello": {
      "number": 1,
      "position": [0]
    },
    "world": {
      "number": 1,
      "position": [7]
    }
  }
 }
}
```

### Bugs

- [x] When only one file put in in a list not in a string
- [x] Case sensitivity when Upper in params
- [x] Fix atomicity since file are handled out of class

### Issues

- [x] New structure 10x slower than previous one (get method is slow?)
- [x] Remove pwuid stuff to be compatible with windows
- [x] Check if file is plain text before adding to list (mime types?)
- [x] Find more elegant solution to handle atomic option (between count and printDict?)
- [x] Check if position are always corrects (words, sequence?)

### Add functionalities

- [x] Give a undifferentiated file or directory (check at init)
- [ ] Give list of non-alpha char to count (Ex: '-')
    here: <https://stackoverflow.com/questions/15753701/how-can-i-pass-a-list-as-a-command-line-argument-with-argparse>
- [x] Add directory recursivity
- [x] Save result as JSON
- [x] Count occurence of a given sequence
- [ ] Count occurence of a given word sequence (Ex: After all, believe in , ...)
- [ ] Pretty print
- [ ] Generate Graphviz (plot)
- [ ] Give list of word or letters to count
- [ ] Store position of word and letters in files
- [ ] Add a quiet mode (don't print parse errors)

### Maintability

- [ ] Improve README
- [x] Improve comments in code
- [ ] Conform to PEP8
- [x] Remove testDir
- [ ] Test with assert and knowed files
- [x] Time and perf benchmarck
- [/] Add a CI pipeline (unittest, code smells, ...)
